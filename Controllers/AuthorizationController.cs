﻿/** 
 * Author: Ahmad J. Hamad
 * 
 */

using Microsoft.AspNetCore.Mvc;
using Authorize.Services.Authorization;
using Authorize.Platform.Config;
using Authorize.Platform.Controllers.Validation;
using Authorize.Models;
using Authorize.Platform.Repository;
using Authorize.Services.Authentication;



/** 
 * 
 */
namespace Authorize.Controllers{

   /** 
    * 
    */
   public class AuthorizationController : Controller{

     /**
      */
     private readonly IAuthorizationFlow authorization;
     private readonly ControllerConfig controllerConfig;
     private readonly UserRepository userRepository;
     private readonly AuthenticationToken authenticationToken;

     /** 
      * 
      */
     public AuthorizationController(IAuthorizationFlow authorization, ControllerConfig controllerConfig, UserRepository userRepository, AuthenticationToken authenticationToken) 
     { 
       this.authorization = authorization;
       this.controllerConfig = controllerConfig;
       this.userRepository = userRepository;
       this.authenticationToken = authenticationToken;
     }

     

    /** 
     * 
     */
    [HttpGet]
    [Route("/authorize")]
    //[AuthorizationControllerAuthorizeValidationAttribute]
    public async Task<JsonResult> Authorize(String code)
    { 
       
      try{
         OAuth2User? oAuth2User = await authorization.Authorize(code);
         User? user = userRepository.CreateUser(oAuth2User!);
         string token = authenticationToken.GenerateJwtToken(user!); 
       }  catch(Platform.PlatformException.ApplicationException e) { 
       }
       //return RedirectToRoute("");
       return  Json(new { ok = 1});
    }
  }
}