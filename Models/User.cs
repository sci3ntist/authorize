﻿/** 
 * Author: Ahmad J. Hamad
 * 
 */


using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



/** 
 * 
 */
namespace Authorize.Models{

  /** 
   * 
   */
  public class User {
    
    [Key]
    [Column("id")]
    public int Id { set; get; }
        
   [Required]
   [Column("email")]
    public string? Email { get; set; }
  }
}