﻿/** 
 * Author: Ahmad J. Hamad
 * 
 */


/** 
 * 
 */
namespace Authorize.Models{

  /** 
   * 
   */
  public class OAuth2User {

     /** 
      * 
      */
     public OAuth2User(string email) => Email = email;

     /** 
      * 
      */ 
     public string? Email { set; get; }
  }
}
