﻿/** 
 * Author: Ahmad J. Hamad 
 */


using Authorize.Platform.Config;
using Authorize.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

/** 
 * 
 */
namespace Authorize.Services.Authentication{
  public class AuthenticationToken{

    /** 
     * 
     */ 
    private readonly JwtConfig jwtConfig;

    /** 
     */
    public AuthenticationToken(JwtConfig jwtConfig) => this.jwtConfig = jwtConfig;


        
    /** 
     * 
     */
    public string GenerateJwtToken(User user)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(jwtConfig.Secret!);
      var tokenDescriptor = new SecurityTokenDescriptor{
        Subject = new ClaimsIdentity(new[] { new Claim("email", user.Email!) }),
        Expires = DateTime.UtcNow.AddDays(7),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
      };
      var token = tokenHandler.CreateToken(tokenDescriptor);
      return tokenHandler.WriteToken(token);
    }
  }
}
