﻿/** 
 *  Author: Ahmad J. Hamad
 */

using Authorize.Models;

/** 
 * 
 */
namespace Authorize.Services.Authorization{
  public interface IAuthorizationFlow {
    public Task<OAuth2User?> Authorize(string code);
  }
}
