﻿/** 
 * Author: Ahmad J. Hamad 
 */


using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Authorize.Platform.Config;
using Authorize.Models;
using Google.Apis.Oauth2.v2;
using Google.Apis.Services;

/** 
 * 
 */
namespace Authorize.Services.Authorization{

  /** 
   * 
   */
  public class GoogleAuthorizationFlow : IAuthorizationFlow {
  
    /** 
     */
    private readonly GoogleConfig googleConfig;

    /** 
     * 
     */       
    public GoogleAuthorizationFlow(GoogleConfig googleConfig)
    {
      this.googleConfig = googleConfig;
    }

    /** 
     * 
     */
    public async Task<OAuth2User?> Authorize(string code)
    {      
      
      GoogleAuthorizationCodeFlow googleAuthorizationCodeFlow = new 
       (new GoogleAuthorizationCodeFlow.Initializer { 
          ClientSecrets = new ClientSecrets
          {
              ClientId = googleConfig.ClientId,
              ClientSecret = googleConfig.ClientSecret
          },
          Scopes = new[] { "profile", "email", "openid" },
          UserDefinedQueryParams = new List<KeyValuePair<string, string>> { 
              new KeyValuePair<string, string>("grant_type","authorization_code")
          },
          
      });


      /** TODO: userId is hardcoded */
      TokenResponse tokenResponse = await googleAuthorizationCodeFlow.ExchangeCodeForTokenAsync( "ahmad@sci3nist.com",code,googleConfig.RedirectUri,CancellationToken.None);
 
      if(tokenResponse!=null) {
        /** TODO: userId is hardcoded */
        UserCredential userCredential = new(googleAuthorizationCodeFlow,"ahmad@sci3ntist.com",tokenResponse);
        Oauth2Service oauth2Service = new(new BaseClientService.Initializer() { 
            HttpClientInitializer = userCredential
        });

        var userInfo = await oauth2Service.Userinfo.Get().ExecuteAsync();
        var email = userInfo.Email;

        return new OAuth2User(email);
      }

      return null;
    }
  }
}
