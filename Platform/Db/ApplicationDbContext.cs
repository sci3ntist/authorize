﻿/** 
 * Author: Ahmad J. Hamad 
 */

using Microsoft.EntityFrameworkCore;
using Authorize.Models;


/** 
 * 
 */
namespace Authorize.Platform.Db{

    /** 
     * 
     */
  public class ApplicationDbContext: DbContext{

    /** 
     */
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

      
    /** 
     */
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<User>().ToTable("user");
    }

    /** 
     * 
     */
    public virtual DbSet<User> Users { get; set; }
  }
}
