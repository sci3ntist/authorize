﻿/** 
 * Author: Ahmad J. Hamad 
 */


/** 
 * 
 */
namespace Authorize.Platform.PlatformException{

  /** 
   * 
   */
  public class ApplicationException: Exception{

   

   /** 
    * 
    */
   public ApplicationException() { }

   /** 
    * 
    */
   public ApplicationException(string message) : base(message) { }


  
   /** 
    * 
    */
   public ApplicationException(Exception originalException):base(originalException.Message) => _originalException = originalException;
   
   /** 
    * 
    */
   private Exception? _originalException;  

   /** 
    * 
    */
   public Exception? originalException { get{ return _originalException;} set{ } }
  }
}
