﻿/** 
 * Author: Ahmad J. Hamad 
 */


using Microsoft.AspNetCore.Mvc.Filters;

/** 
 * 
 */
namespace Authorize.Platform.Controllers.Validation{
  public class AuthorizationControllerAuthorizeValidationAttribute: ActionFilterAttribute {

    /** 
     * 
     */
    public override void OnActionExecuting(ActionExecutingContext context)
    {
      base.OnActionExecuting(context);
    }
  }
}