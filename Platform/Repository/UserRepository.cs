﻿/** 
 * Author: Ahmad J. Hamad 
 */

using Authorize.Platform.Db;
using Authorize.Models;
using Microsoft.EntityFrameworkCore;



/** 
 * 
 */
namespace Authorize.Platform.Repository{

  /** 
   * 
   */
  public class UserRepository {

     /** 
      * 
      */
     private ApplicationDbContext applicationDbContext;

     /** 
      * 
      */
     public UserRepository(ApplicationDbContext applicationDbContext) => this.applicationDbContext = applicationDbContext;

     /** 
      * 
      */
     public User? CreateUser(OAuth2User auth2User)
     {
        User user = new(){
          Email = auth2User.Email
        };
        
        /** 
         * 
         */     
        if(applicationDbContext.Users.Where(u => u.Email == user.Email).FirstOrDefault() == null) { 
          applicationDbContext.Add<User>(user);
          try { 
            applicationDbContext.SaveChanges();
          } catch(DbUpdateException dbUpdateException) { 
            throw new PlatformException.ApplicationException(dbUpdateException);
          }
        }

        return user;
     }
  }
}
