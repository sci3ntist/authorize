﻿/** 
 * Author: Ahmad J. Hamad
 * 
 */


/** 
 * 
 */
namespace Authorize.Platform.Config{
  public class ControllerConfig {
    public string? SuccessfullAuthenticationRedirectToUrl {set;get;}
    public string? FailureAuthenticationRedirectToUrl {set;get;}
  }
}
