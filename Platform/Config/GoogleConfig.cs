﻿/** 
 * Author: Ahmad J. Hamad
 * 
 */


/** 
 * 
 */
namespace Authorize.Platform.Config{

  /** 
   * 
   */
  public class GoogleConfig{
     public string? ClientId {set;get;}
     public string? ClientSecret { set;get;}
     public string? RedirectUri { set;get;}
  }
}
