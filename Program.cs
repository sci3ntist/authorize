/** Define namespaes */
using Authorize.Services.Authorization;
using Authorize.Platform.Config;
using Authorize.Platform.Db;
using Microsoft.EntityFrameworkCore;
using Authorize.Platform.Repository;
using Authorize.Services.Authentication;




var builder = WebApplication.CreateBuilder(args);


builder.Services.AddRazorPages();

/** Create dependency injection */
builder.Services.AddTransient<IAuthorizationFlow, GoogleAuthorizationFlow>();
builder.Services.AddTransient<AuthenticationToken>();
builder.Services.AddTransient<GoogleConfig>(_ => new GoogleConfig {   ClientId = builder.Configuration["Google:ClientId"], 
                                                                      ClientSecret = builder.Configuration["Google:ClientSecret"] ,
                                                                      RedirectUri = builder.Configuration["Google:RedirectUri"] }
                                                                    );

builder.Services.AddTransient<ControllerConfig>(_ => new ControllerConfig {  SuccessfullAuthenticationRedirectToUrl = builder.Configuration["Google:ClientId"], 
                                                                             FailureAuthenticationRedirectToUrl = builder.Configuration["Google:ClienSecret"] }
                                                                         );
builder.Services.AddTransient<JwtConfig>(_ => new JwtConfig {  Secret= builder.Configuration["JWT:Secret"]});

builder.Services.AddTransient<UserRepository>();



/** Define database */
builder.Services.AddDbContextPool<ApplicationDbContext>(options => options.UseMySql(builder.Configuration.GetConnectionString("MySqlConnectionString"),new MySqlServerVersion(new Version(5, 6, 36))));

var app = builder.Build();


if(!app.Environment.IsDevelopment()){
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
